(function (Drupal) {

    let initStripeHasRun = false;

    Drupal.behaviors.stripeWebformPayment = {
        attach: function (context, settings) {
            initStripe(context, settings);
        },
    };

    function initStripe(context, settings) {

        const paymentStep = context.querySelector('.stripe-webform-payment-container');
        if (!paymentStep) return;

        if (initStripeHasRun) return;
        initStripeHasRun = true;

        const {
            publishableKey, clientSecret, webformId, clientEmail,
            clientName, collectAddress, addressType, appearanceEnabled, paymentAppearance,
        } = drupalSettings.stripeWebformPayment;

        const stripe = Stripe(publishableKey);
        const webform = document.querySelector(`[data-stripe-form-id="${webformId}"]`);
        const submitButton = context.querySelector("[data-stripe-submit='stripe-webform-submit-button']");

        let buttonClicked = false;
        if (!webform || !submitButton) return;

        // Hide the original submit button; instead use a separate payment
        // button, which will not trigger AJAX events.
        const payButton = submitButton.cloneNode(false);
        payButton.addEventListener('click', async (e) => {
          // Do not submit the form.
          e.preventDefault();

          // Handle payment as long as it has not already been handled.
          if (!paymentHandled) {
            handlePayButtonClick();
          }
        });

        submitButton.disabled = true;
        submitButton.style.display = 'none';
        submitButton.insertAdjacentElement('beforebegin', payButton);

        // Create paymentElementOptions and set appearance if appearanceEnabled is true
        const paymentElementOptions = { clientSecret };
        if (appearanceEnabled) {
            paymentElementOptions.appearance = paymentAppearance;
        }

        const elements = stripe.elements(paymentElementOptions);

        const paymentElement = elements.create('payment', paymentElementOptions);
        paymentElement.mount('#payment-element');

        if (collectAddress !== null) {
            const addressElement = elements.create("address", { mode: addressType });
            addressElement.mount("#address-element");
        }

        paymentElement.on('change', event => payButton.disabled = event.empty || event.error);
        let paymentHandled = false;

        async function handlePayButtonClick() {
            if (paymentHandled) return;
            setLoading(true);

            handlePayment()
                .then(submitWebform)
                .catch((error) => {
                    showMessage(error.message, 'error');
                    setLoading(false);
                    resetButtonAndVariables();
                });
        }

        function resetButtonAndVariables() {
            payButton.disabled = false;
            payButton.setAttribute('data-payment-handled', 'false');
        }

        async function handlePayment() {
            const { error, paymentIntent } = await stripe.confirmPayment({
                elements,
                confirmParams: { receipt_email: clientEmail },
                metadata: { customer_name: clientName },
                redirect: 'if_required',
            });

            if (error) {
                throw new Error(error.message);
            } else {
                await handlePaymentIntentStatus(paymentIntent, submitWebform);
                paymentHandled = true;
            }
        }

        async function handlePaymentIntentStatus(paymentIntent, submitWebform) {
            if (paymentIntent.status === "succeeded") {
                submitWebform();
            } else if (paymentIntent.status === "requires_action") {
                let paymentComplete = false;
                do {
                    const updatedPaymentIntent = await stripe.retrievePaymentIntent(paymentIntent.id);
                    if (updatedPaymentIntent.status === 'succeeded') {
                        paymentComplete = true;
                        submitWebform();
                    } else {
                        await new Promise(resolve => setTimeout(resolve, 500));
                    }
                } while (!paymentComplete);
            }
        }

        let formSubmitted = false;
        function submitWebform() {
            if (!formSubmitted) {
                formSubmitted = true;
                paymentHandled = true;
                submitButton.disabled = false;
                submitButton.click();
                submitButton.disabled = true;

            }
        }

        function showMessage(messageText, messageType) {
            const messageContainer = document.querySelector('#payment-message');
            const messageContent = messageContainer.querySelector('.messages__content');

            messageContainer.classList.remove('hidden');
            messageContent.textContent = messageText;
            messageContainer.classList.add('messages', `messages--${messageType}`);
        }
        function setLoading(isLoading) {
            payButton.disabled = isLoading;
            payButton[payButton.tagName.toLowerCase() === 'input' ? 'value' : 'textContent'] = isLoading ? 'Processing payment...' : 'Submit';
        }
    }
})(Drupal);